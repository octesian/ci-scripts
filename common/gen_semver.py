#!/usr/bin/env python3
import os
import re
import sys
import semver
import subprocess
from os.path import join


def git(*args):
    return subprocess.check_output(["git"] + list(args))


def tag_repo(old_version, new_version):
    print(f"Old version: {old_version}\nnew_version: {new_version}")
    url = os.environ["CI_REPOSITORY_URL"]

    # Transforms the repository URL to the SSH URL
    # Example input: https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@gitlab.com/threedotslabs/ci-examples.git
    # Example output: git@gitlab.com:threedotslabs/ci-examples.git
    push_url = re.sub(r'.+@([^/]+)/', r'git@\1:', url)
    
    git("config", "--global", "user.email", "bot@example.com")
    git("config", "--global", "user.name", "bot")
    git("remote", "set-url", "--push", "origin", push_url)
    subprocess.check_output(
        [
            "bump2version",
            "--current-version",
            old_version,
            "--new-version",
            new_version,
            "major",
            "--no-sign-tags",
        ]
    )
    git("push", "origin", "--tags")
    git("push", "origin")


def get_version(latest, message):
    print(f"Current version: {latest}\nCommit message: {message}")
    if "fix(" in message or "patch(" in message:
        print("Bumping patch")
        return semver.bump_patch(latest)
    elif "feat(" in message or "minor(" in message:
        print("Bumping minor")
        return semver.bump_minor(latest)
    elif "perf(" in message or "major(" in message:
        print("Bumping major")
        return semver.bump_major(latest)
    else:
        return latest


def __list_converter() -> list:
    return []


def main():
    latest = None
    version = None
    try:
        latest = git("describe", "--tags", "--abbrev=0").decode('utf-8').strip()
        print(f"Tagging repo with {latest}")
        message = git("log", "-1", "--pretty=%B").decode('utf-8')
        version = get_version(latest, message)
    except subprocess.CalledProcessError as e:
        print(e)
        # No tags in the repository
        version = "0.0.1"
        latest = "0.0.0"

    print(f"Version: {version}")
    tag_repo(latest, version)


if __name__ == "__main__":
    sys.exit(main())
