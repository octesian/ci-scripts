from unittest import TestCase
from common.gen_semver import get_version


class TestGenSemver(TestCase):
    def test_get_version(self):
        v = get_version("0.0.0", "fix(blah):")
        self.assertEqual(v, "0.0.1")
        v = get_version("0.0.0", "feat(blah):")
        self.assertEqual(v, "0.1.0")
        v = get_version("0.0.0", "perf(blah):")
        self.assertEqual(v, "1.0.0")
